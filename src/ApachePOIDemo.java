import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.swing.JFileChooser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Shailesh Sonare
 */
public class ApachePOIDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] shailesh) throws IOException {
        
        /*
        
        Workbook workbook = new HSSFWorkbook();
        
        Sheet sheet1 = workbook.createSheet("movies");
//        Sheet sheet2 = workbook.createSheet("Test Cases");
//        Sheet sheet3 = workbook.createSheet(WorkbookUtil.createSafeSheetName("$*(^&?"));
        
        Row row = sheet1.createRow(0);
        
//        Cell cell = row.createCell(4);
//        cell.setCellValue("Hello World");
//        Cell cell2 = row.createCell(3);
//        cell2.setCellValue("terminator");
//        System.out.println(cell.getRichStringCellValue().toString());
//        System.out.println(cell2.getRichStringCellValue().toString());

        Cell cell1 = row.createCell(0);
        Cell cell2 = row.createCell(1);
        Cell cell3 = row.createCell(2);
        Cell cell4 = row.createCell(3);
        Cell cell5 = row.createCell(4);
        
        cell1.setCellValue(5);
        cell2.setCellValue("+");
        cell3.setCellValue(6);
        cell4.setCellValue("=");
        cell5.setCellFormula("A1+C1");
        
        try {
            FileOutputStream outputStream = new FileOutputStream("test1.xlsx");
            workbook.write(outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        /*
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showDialog(null, "Select File");
        
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            Workbook workbook = new HSSFWorkbook(new FileInputStream(fileChooser.getSelectedFile()));
            
            Sheet sheet = workbook.getSheetAt(0);
            
            for(Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();){
                for(Iterator<Cell> cit = rit.next().cellIterator(); cit.hasNext();){
                    System.out.println(cit.next());
                }
                System.out.println("");
            }
        } else {
            System.out.println("Invalid output");
        }
        */
        
        /*
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showDialog(null, "Select File");
        
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            FileInputStream excelFile = new FileInputStream(fileChooser.getSelectedFile());
            Workbook workbook = new XSSFWorkbook(excelFile);
            
            Sheet sheet = workbook.getSheetAt(0);
            
            for(Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
                for(Iterator<Cell> cit = rit.next().cellIterator(); cit.hasNext();) {
                    System.out.print(cit.next() + "\t" + cit.next());
                }
                System.out.println("");
            }
        } 
        */
        
        Workbook  workbook = new HSSFWorkbook(new FileInputStream(new File("test1.xls")));
        Sheet sheet = workbook.getSheetAt(0);
        
        for(Iterator<Row> itr = sheet.rowIterator(); itr.hasNext();) {
                for(Iterator<Cell> itc = itr.next().cellIterator(); itc.hasNext();) {
                    try {
                        System.out.println(itc.next());
                    } catch(NoSuchElementException nsee) {
                        System.out.println("hi");
                    } catch(Exception e) {
                        System.out.println("hello");
                    }
                    
                    try {
                        System.out.println(itc.next());
                    } catch(NoSuchElementException nsee) {
                        System.out.println("hi");
                    } catch(Exception e) {
                        System.out.println("hello");
                    }
                }
            }
        
    }    
}
